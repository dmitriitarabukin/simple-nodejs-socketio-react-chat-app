const utilities = {
	doesUserExist: function doesUserExist(username, users) {
		return users.some(user => user.username === username);
	}
}

module.exports = utilities;