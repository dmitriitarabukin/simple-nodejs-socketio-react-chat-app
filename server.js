require("dotenv").config();
const express = require("express");
const app = require("express")();
const server = require("http").createServer(app);
const io = require("socket.io")(server);
const port = process.env.PORT || 3030;
const utilities = require("./utilities");

// [{ room_id: '', username: '' }, ...]
let users_online = [];

io.on("connect", onSocketConnect);

function onSocketConnect(socket) {
	socket.on("join chat", ({ room_id, username }) => {
		users_online.push({ room_id, username });

		socket.join(room_id);

		socket.to(room_id).emit("user joined", username);

		let users_online_in_room = users_online
			.filter(user => user.room_id == room_id)
			.map(user => user.username);

		socket.emit("users online", users_online_in_room);

		console.log(`User ${username} joined the chat`);
	});

	socket.on("chat message", ({ room_id, username, message }) => {
		// convert date to UTC so the clients could convert it back to their local time
		let date = new Date().toUTCString();
		socket.to(room_id).emit("chat message", { username, message, date });
	});

	socket.on("log out", ({ room_id, username }) => {
		users_online = users_online.filter(user => user.username !== username);

		socket.to(room_id).emit("user disconnected", username);

		socket.disconnect(true);

		console.log(`User ${username} disconnected from chat`);
	});
}

app.use(express.json());
app.post("/doesUserExist", (req, res) => {
	let username = req.body.username,
		does_user_exist = utilities.doesUserExist(username, users_online);

	res.send({ does_user_exist });
});

if (process.env.NODE_ENV === "production") {
	const path = require("path");
	app.use("/", express.static(path.resolve(__dirname, "./frontend/build")));
	app.get("*", (req, res) => {
		res.sendFile(path.resolve(__dirname, "./frontend/build", "index.html"));
	});
}

server.listen(port, () => {
	console.log(`Server listening on port: ${port}`);
});
