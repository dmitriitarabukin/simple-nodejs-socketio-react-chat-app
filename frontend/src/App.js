import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Chat from "./components/Chat";
import ChatEntry from "./components/ChatEntry";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from "react-bootstrap";

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Container>
            <Switch>
              <Route exact path="/" component={ChatEntry} />
              <Route exact path="/room/:id" component={Chat} />
            </Switch>
          </Container>
        </div>
      </Router>
    );
  }
}

export default App;
