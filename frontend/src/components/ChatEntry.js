import React from "react";
import { Redirect } from "react-router-dom";
import axios from "axios";
import { Col, Row } from "react-bootstrap";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import ChatToast from "./ChatToast";

class ChatEntry extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			username: "",
			room_id: this.props.location.state
				? this.props.location.state.room_id
				: null,
			to_chat: false,
			toast: {
				show: false,
				message: ""
			}
		};
	}

	changeName = event => {
		this.setState({
			username: event.target.value
		});
	};

	checkIfUserExists = async () => {
		try {
			let response = await axios.post("/doesUserExist", {
				username: this.state.username
			});
			return response.data.does_user_exist;
		} catch (error) {
			console.log(error);
		}
	};

	validateUsername = () => {
		if (this.state.username.length > 0) return true;
		return false;
	};

	tryEnterChatRoom = async event => {
		event.preventDefault();

		if (!this.validateUsername()) {
			this.toggleToast(true, "Username should be at least 1 character long");
			return;
		}

		let does_user_exist = await this.checkIfUserExists();
		if (does_user_exist) {
			this.toggleToast(
				true,
				`User with name "${this.state.username}" already exists.`
			);
			return;
		}

		// if user arrived from link - room id provided by the link will be used, else - new room id will be created
		this.setState({
			room_id:
				this.state.room_id ||
				String(Math.floor(Math.random() * 100) + Date.now()),
			to_chat: true
		});
	};

	// TODO: create HOC with toggleToast?
	toggleToast = (show, message) => {
		this.setState({
			toast: {
				show,
				message
			}
		});
	};

	render() {
		let { to_chat, room_id, username, toast } = this.state;

		// when user fills in his or her name, they will be redirected to the chat room
		if (to_chat) {
			return (
				<Redirect
					to={{
						pathname: `/room/${room_id}`,
						state: { username }
					}}
				/>
			);
		}

		return (
			<Row style={{ height: "100vh", display: "flex", alignItems: "center" }}>
				<Col xs={12} md={{ span: 3, offset: 2 }}>
					<h1>Chat App</h1>
					<Form onSubmit={this.tryEnterChatRoom}>
						<Form.Group controlId="formUserName">
							<Form.Label>Enter your name to join chat room:</Form.Label>
							<Form.Control type="text" onChange={this.changeName} />
						</Form.Group>
						<Button variant="primary" type="submit">
							Join
						</Button>
					</Form>
				</Col>
				<ChatToast
					show={toast.show}
					toggleToast={this.toggleToast}
					message={toast.message}
				/>
			</Row>
		);
	}
}

export default ChatEntry;
