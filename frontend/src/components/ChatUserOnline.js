import React from "react";

export default ({ user }) => {
	return (
		<p style={{ marginBottom: "0.5rem" }}>
			<span>{user}</span>
		</p>
	);
};
