import React, { Component } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";

class ChatInput extends Component {
	constructor(props) {
		super(props);
		this.state = { message: "" };
	}

	setMessage = event => {
		this.setState({ message: event.target.value });
	};

	render() {
		let { message } = this.state;

		return (
			<Form
				style={{ marginBottom: "25px" }}
				onSubmit={event => {
					event.preventDefault();
					this.props.onSubmitMessage(message);
					this.setState({ message: "" });
				}}
			>
				<Form.Group>
					<Form.Control
						type="text"
						placeholder="Enter message..."
						value={message}
						onChange={this.setMessage}
						size="sm"
					/>
				</Form.Group>
				<Button
					variant="primary"
					type="submit"
					style={{ marginRight: "10px", marginBottom: "5px" }}
				>
					Send
				</Button>
				<Button
					variant="outline-secondary"
					style={{ marginBottom: "5px" }}
					onClick={this.props.copyChatLink}
				>
					Copy chat link
				</Button>
			</Form>
		);
	}
}

export default ChatInput;
