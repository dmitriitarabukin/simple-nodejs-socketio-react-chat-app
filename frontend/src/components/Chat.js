import React from "react";
import { Redirect } from "react-router-dom";
import ChatUsername from "./ChatUsername";
import ChatUserOnline from "./ChatUserOnline";
import ChatInput from "./ChatInput";
import ChatMessage from "./ChatMessage";
import ChatToast from "./ChatToast";
import io from "../../../node_modules/socket.io-client/dist/socket.io.js";
import { Col, Row } from "react-bootstrap";

class Chat extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			username: this.props.location.state
				? this.props.location.state.username
				: "",
			room_id: this.props.match.params.id,
			messages: [],
			users_online: [],
			toast: {
				show: false,
				message: ""
			}
		};
	}

	componentDidMount() {
		if (this.state.username) {
			// clean up component after page refresh so users won't be duplicated
			window.addEventListener("beforeunload", this.componentCleanup);

			this.socket = io(window.location.origin);

			this.socket.on("connect", this.joinChat);

			this.socket.on("users online", this.setUsersOnline);

			this.socket.on("user joined", this.addUser);

			this.socket.on("chat message", this.addMessage);

			this.socket.on("user disconnected", this.removeUser);
		}
	}

	joinChat = () => {
		this.socket.emit("join chat", {
			room_id: this.state.room_id,
			username: this.state.username
		});
	};

	setUsersOnline = users_online => {
		this.setState({
			users_online
		});
	};

	addUser = username => {
		this.setState(state => ({
			users_online: [...state.users_online, username]
		}));
	};

	removeUser = logged_out_username => {
		this.setState(state => ({
			users_online: state.users_online.filter(
				username => username !== logged_out_username
			)
		}));
	};

	addMessage = (message, convert_time = true) => {
		// server returns UTC date, convert it to local
		if (convert_time)
			message.date = new Date(message.date).toLocaleTimeString();
		this.setState(state => ({ messages: [message, ...state.messages] }));
	};

	submitMessage = message_string => {
		// on submitting ChatInput form, send the message and add it to the list
		this.socket.emit("chat message", {
			room_id: this.state.room_id,
			username: this.state.username,
			message: message_string
		});

		this.addMessage(
			{
				username: this.state.username,
				message: message_string,
				date: new Date().toLocaleTimeString()
			},
			false // don't convert time
		);
	};

	copyChatLink = async () => {
		let current_url = window.location.href;
		try {
			await navigator.clipboard.writeText(current_url);
			this.toggleToast(true, "Click right mouse button and paste the link!");
		} catch (error) {
			this.toggleToast(true, "There was an error copying chat link.");
			console.log(error);
		}
	};

	// TODO: create HOC with toggleToast?
	toggleToast = (show, message) => {
		this.setState({
			toast: {
				show,
				message
			}
		});
	};

	componentCleanup = () => {
		// disconnects socket and cleans up component
		this.socket.emit("log out", {
			room_id: this.state.room_id,
			username: this.state.username
		});
		this.setState({
			username: ""
		});
		this.socket.close();
	};

	render() {
		let { username, room_id, messages, users_online, toast } = this.state;

		if (!username) {
			return <Redirect to={{ pathname: "/", state: { room_id } }} />;
		}

		return (
			<>
				<Row style={{ paddingTop: "1.5rem" }}>
					<Col xs={12}>
						<h1>Chat App</h1>
						<hr />
					</Col>
				</Row>
				<Row style={{ paddingTop: "1.5rem" }}>
					<Col xs={12} md={3}>
						<ChatUsername username={username} />
						<ChatInput
							onSubmitMessage={messageString => {
								this.submitMessage(messageString);
							}}
							copyChatLink={this.copyChatLink}
						/>
					</Col>
					<Col
						xs={6}
						md={4}
						style={{ height: "calc(100vh - 8.5rem)", overflow: "auto" }}
					>
						<strong style={{ display: "inline-block", marginBottom: "10px" }}>
							Messages:
						</strong>
						{messages.map((message, index) => (
							<ChatMessage
								key={index}
								message={message.message}
								username={message.username}
								date={message.date}
							/>
						))}
					</Col>
					<Col xs={6} md={2}>
						<strong style={{ display: "inline-block", marginBottom: "10px" }}>
							Users <span style={{ color: "green" }}>online</span>:
						</strong>
						{users_online.map((user, index) => (
							<ChatUserOnline key={index} user={user} />
						))}
					</Col>
					<ChatToast
						show={toast.show}
						toggleToast={this.toggleToast}
						message={toast.message}
					/>
				</Row>
			</>
		);
	}
}

export default Chat;
