import React from "react";

export default ({ username, message, date }) => {
	return (
		<p>
			<strong>{username}</strong> <small>({date})</small>:&nbsp;
			<em style={{ wordBreak: "break-word" }}>{message}</em>
		</p>
	);
};
