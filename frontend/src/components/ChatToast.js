import React from "react";
import Toast from "react-bootstrap/Toast";

export default ({ show, toggleToast, message }) => {
	return (
		<Toast
			onClose={() => toggleToast(false, message)}
			style={{ position: "fixed", top: "25px", right: "25px" }}
			show={show}
			delay={6000}
			autohide
		>
			<Toast.Body>{message}</Toast.Body>
		</Toast>
	);
};
