import React from "react";

export default ({ username }) => {
	return (
		<strong style={{ display: "inline-block", marginBottom: "10px" }}>
			{username}
		</strong>
	);
};
